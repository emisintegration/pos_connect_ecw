# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import datetime
import csv
from openerp.exceptions import UserError
import time
from openerp.tools.translate import _
from dateutil import tz

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('Asia/Riyadh')
    
class pos_category(osv.osv):
    _inherit = "pos.category"
    
    _columns = {
           'emis_id': fields.char('EMIS Product ID')     
    }

class res_partner(osv.osv):
    _inherit = "res.partner"
    
    _columns = {
           'emis_id': fields.char('EMIS Customer ID')     
    }
    

class res_users(osv.osv):
    _inherit = "res.users"
    
    _columns = {
           'emis_id': fields.char('EMIS USER ID')     
    }

class pos_session(osv.osv):
    _inherit = "pos.session"
    
    _columns = {
           'is_synced': fields.boolean('Is Synced?'),   
    }
    
    _defaults = {
        'is_synced': lambda *a: False,
    }
    
class pos_config(osv.osv):
    _inherit = "pos.config"
    
    _columns = {
           'super_user_id':fields.many2one('res.users',"Supervisor"),
    }
    
class pos_order(osv.osv):
    _inherit = "pos.order"
      
    _columns = {
        'is_synced':fields.boolean('Is Synced?'),
        'order_parent_id':fields.many2one('pos.order', "Parent ID")
    }
    
    _defaults = {
        'is_synced': lambda *a: False,
    }
    
    def refund(self, cr, uid, ids, context=None):
        """Create a copy of order for refund order"""
        clone_list = []
        line_obj = self.pool.get('pos.order.line')
        
        for order in self.browse(cr, uid, ids, context=context):
            current_session_ids = self.pool.get('pos.session').search(cr, uid, [
                ('state', '!=', 'closed'),
                ('user_id', '=', uid)], context=context)
            if not current_session_ids:
                raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

            clone_id = self.copy(cr, uid, order.id, {
                'name': order.name + ' REFUND', # not used, name forced by create
                'session_id': current_session_ids[0],
                'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
                'order_parent_id':order.id
            }, context=context)
            clone_list.append(clone_id)

        for clone in self.browse(cr, uid, clone_list, context=context):
            for order_line in clone.lines:
                line_obj.write(cr, uid, [order_line.id], {
                    'qty': -order_line.qty
                }, context=context)

        abs = {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id':clone_list[0],
            'view_id': False,
            'context':context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return abs
    
class ProductTemplate(osv.osv):
    _inherit = "product.template"
      
    _columns = {
        'bom_detail':fields.one2many('product.bom.detail', 'parent_id', 'BoM Details', copy=True),
        'emis_uom':fields.char('uom'),
        'emis_categ_id':fields.char('EMIS Category')
    }
    
class product_bom_detail(osv.osv):
    _name = "product.bom.detail"
    
    _columns = {
                'product_id': fields.many2one('product.template', 'Product', required=True, change_default=True),
                'qty': fields.float('Quantity'),
                'uom':fields.char('UOM'),
                'parent_id': fields.many2one('product.template', 'Parent Product', ondelete='cascade', domain=[('sale_ok', '=', True)] ),
                'bom_coice':fields.one2many('product.bom.choice', 'parent_id', 'BoM Choice', copy=True),
                'has_choice':fields.boolean('Has Choice?')
                }
    
    _defaults = {
        'qty': lambda *a: 1,
        'has_choice':lambda *a: False,
    }
    
class product_bom_choice(osv.osv):
    _name = "product.bom.choice"
    
    _columns = {
                'product_id': fields.many2one('product.template', 'Product', required=True, change_default=True),
                'qty': fields.float('Quantity'),
                'uom':fields.char('UOM'),
                'parent_id': fields.many2one('product.template', 'Parent Product', ondelete='cascade'),
                }
    
    _defaults = {
        'qty': lambda *a: 1,
    }
    
class AccountJournal(osv.osv):
    _inherit = "account.journal"
     
    _columns = {
                'card_type': fields.boolean('Card Type?'),
                'emis_id': fields.char('EMIS Id'),
                }
    
class emis_connect(osv.osv):
    _name = "emis.connect"
    
    _columns = {
        'name': fields.char('Code'),
    }
    
    def sync_data(self, cr, uid, ids=False, context=None):
        BUSINESS_UNIT   =   "6000"
        UNIT_ID         =   "01533"
        SHIFT_PFX       =   "H298"   
        SALES_PFX       =   "C298"
        RETURN_PFX      =   "N298"
        SUPERVISOR      =   "6000001842"
        SUPERVISOR_PWD  =   "JEFFREY1842"
        
        print "CSV Export Begins..."
        # SALE ORDER DATA UPLOAD BEGINS 
        
        session_obj = self.pool['pos.session']
        query = "SELECT DISTINCT start_at::date FROM pos_session where (state = 'closed') AND (is_synced = False) order by start_at"
        cr.execute(query)
        session_date_list = cr.fetchall()
        session_dates = [element for tupl in session_date_list for element in tupl]
        
        card_ids = self.pool.get('account.journal').search(cr,uid,[('card_type','=',True)])
        
        with open('OL1_MF_SO_HD_TMP.csv', 'wb') as salecsvfile , \
            open('OL1_MF_SO_LN_TMP.csv', 'wb') as linecsvfile, \
            open('OL1_MF_SO_LN_DETAIL_TMP.csv','wb') as linedetailfile, \
            open('OL1_MF_SO_PAY_TMP.csv', 'wb') as paymentcsvfile, \
            open('OL1_MF_SHIFT_TRACKING_TMP.csv', 'wb') as shifttrackingfile, \
            open('OL1_MF_SHIFT_MANHOUR_HD_TMP.csv', 'wb') as super_shift_closing_file, \
            open('OL1_MF_UNIT_SHIFT_HD_TMP.csv', 'wb') as super_shift_opening_file, \
            open('OL1_MF_UNIT_SHIFT_OPEN_TMP.csv', 'wb') as cashier_shift_opening_file, \
            open('OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv', 'wb') as cashier_shift_open_emp_file, \
            open('OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv', 'wb') as cashier_shift_closing_file, \
            open('OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv', 'wb') as credit_card_hd_file, \
            open('OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv', 'wb') as credit_card_ln_file :
            
            try:
                ################# order
                
                fieldnames = ('SOH_BUS_UNIT', 'SOH_PFX', 'SOH_NO','SOH_FROM','SOH_CODE','SOH_SALES_TYPE','SOH_CLS',
                              'SOH_TYPE','SOH_DATE','SOH_YEAR','SOH_PERIOD','SOH_CUSTOMER','SOH_CUSTOMER_NAME','SOH_CUSTOMER_EMP',
                              'SOH_TERMS','SOH_CURRENCY','SOH_EXCHANGE_DATE','SOH_EXCHANGE_RATE','SOH_EMPLOYEE','SOH_SALESPERSON',
                              'SOH_WHSE','SOH_POST','SOH_SHIFT_PFX','SOH_SHIFT_NO','SOH_DISC_CARD','SOH_DISC_PCT','SOH_DISC_AMT',
                              'SOH_INV_DISC_FLAG','SOH_INV_DISC_PCT','SOH_LEFT_CASH','SOH_LEFT_RETURN','SOH_CASH_GROUP','SOH_RET_BUS_UNIT',
                              'SOH_RET_UNIT','SOH_RET_INV_PFX','SOH_RET_INV_NO','SOH_EXCLUDE_CARD_CHRG','SOH_CRE_BY','SOH_CRE_DATE',
                              'SOH_NOTES','SOH_CREDIT_HOLD','SOH_HOLD_REASON','SOH_OVER_DUE_AMT','SOH_PRINT_DATE','SOH_AR_DOC_PFX',
                              'SOH_AR_DOC_NO','SOH_DISC_LOCK','SOH_REFRESHED','SOH_ALLOW_MODIFY','SOH_REFRESH_DATE','SOH_SALESMAN',
                              'SOH_DIR_DEL_FLAG','SOH_SV_CUSTOMER','SOH_PIN_CODE','SOH_CENTRAL_DISC_CARD','SOH_COUNTER_NUMBER',
                              'SOH_SHIFT_NUMBER')
            
                writer = csv.DictWriter(salecsvfile, fieldnames=fieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                headers = dict( (n,n) for n in fieldnames )
                writer.writerow(headers)
                ################## line
                
                linefieldnames = ('SOL_BUS_UNIT', 'SOL_PFX', 'SOL_NO','SOL_LN','SOL_BAR_CODE','SOL_ITEM','SOL_ITEM_REV',
                      'SOL_WHSE','SOL_QTY','SOL_UOM','SOL_UOM_STOCK','SOL_FACTOR','SOL_COST','SOL_PRICE',
                      'SOL_DISC_PCT','SOL_FREE_QTY','SOL_CNS_PFX','SOL_CNS_NO','SOL_CNS_LN','SOL_CRE_BY',
                      'SOL_CRE_DATE','SOL_NOTES','SOL_SALESMAN','SOL_DISC_AMT','SOL_PRODUCTION_FLAG','SOL_PRODUCTION_QTY'
                     )

                line_writer = csv.DictWriter(linecsvfile, fieldnames=linefieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                line_headers = dict( (fn,fn) for fn in linefieldnames )
                line_writer.writerow(line_headers)
                
                ################## line detail
                               
                linedetailfieldnames = ('SOLD_BUS_UNIT', 'SOLD_UNIT', 'SOLD_WHSE','SOLD_SHIFT_NO','SOLD_PFX','SOLD_NO','SOLD_LN',
                      'SOLD_LN1','SOLD_BAR_CODE','SOLD_ITEM','SOLD_ITEM_REV','SOLD_ITEM_CAT','SOLD_QTY','SOLD_UOM',
                      'SOLD_UOM_STOCK','SOLD_FACTOR','SOLD_COST','SOLD_PRICE','SOLD_BOM_ITEM','SOLD_BOM_ITEM_REV',
                      'SOLD_BOM_UOM','SOLD_BOM_QTY','SOLD_BOM_TOTQTY','SOLD_BOM_COST','SOLD_TYPE','SOLD_CLS','SOLD_SHIFT_PFX',
                      'SOLD_SHIFT_DATE','SOLD_YEAR','SOLD_PERIOD'
                     )

                linedetail_writer = csv.DictWriter(linedetailfile, fieldnames=linedetailfieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                linedetail_headers = dict( (fn,fn) for fn in linedetailfieldnames )
                linedetail_writer.writerow(linedetail_headers)
                
                ################# payment
                
                paymentfieldnames = ('PAY_BUS_UNIT', 'PAY_PFX', 'PAY_NO','PAY_LN','PAY_PAY','PAY_CURRENCY','PAY_EXCHANGE_DATE',
                      'PAY_EXCHANGE_RATE','PAY_FC_VALUE','PAY_AMT','PAY_CRED_CARD_CHRG','PAY_AMT_GL','PAY_LC_CHANGE','PAY_CARD_NO','PAY_CARD_TRANS_NO',
                      'PAY_PERSON_NAME','PAY_NATIONALITY','PAY_PASSPORT_NO','PAY_FLIGHT_NO','PAY_CHECK_NO','PAY_CHECK_DATE',
                      'PAY_CRE_BY','PAY_CRE_DATE','PAY_NOTES'
                     )

                payment_writer = csv.DictWriter(paymentcsvfile, fieldnames=paymentfieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                payment_headers = dict( (n,n) for n in paymentfieldnames )
                payment_writer.writerow(payment_headers)
                
                ################# super_shift_opening
                
                super_shift_opening_fields = ('SHFTH_BUS_UNIT', 'SHFTH_PFX', 'SHFTH_NO','SHFTH_UNIT','SHFTH_SHIFT','SHFTH_DATE','SHFTH_YEAR',
                      'SHFTH_PERIOD','SHFTH_SV_EMPLOYEE','SHFTH_PASSWORD','SHFTH_LEFT_CASH','SHFTH_POST','SHFTH_CRE_BY','SHFTH_CRE_DATE','SHFTH_NOTES'
                     )

                super_shift_opening_writer = csv.DictWriter(super_shift_opening_file, fieldnames=super_shift_opening_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                super_shift_opening_headers = dict( (n,n) for n in super_shift_opening_fields )
                super_shift_opening_writer.writerow(super_shift_opening_headers)
                
                ################# cashier_shift_opening
                
                cashier_shift_opening_fields = ('SHFTO_BUS_UNIT', 'SHFTO_PFX', 'SHFTO_NO','SHFTO_EMP','SHFTO_LN','SHFTO_PAYMENT','SHFTO_CURRENCY',
                      'SHFTO_EXCHANGE_DATE','SHFTO_EXCHANGE_RATE','SHFTO_AMT','SHFTO_AMT_GL','SHFTO_CRE_BY','SHFTO_CRE_DATE','SHFTO_NOTES'
                     )

                cashier_shift_opening_writer = csv.DictWriter(cashier_shift_opening_file, fieldnames=cashier_shift_opening_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_opening_headers = dict( (n,n) for n in cashier_shift_opening_fields )
                cashier_shift_opening_writer.writerow(cashier_shift_opening_headers)
                
                ################# cashier_shift_opening_emp
                
                cashier_shift_open_emp_fields = ('SHFTE_BUS_UNIT', 'SHFTE_PFX', 'SHFTE_NO','SHFTE_EMP','SHFTE_POST','SHFTE_CRE_BY','SHFTE_CRE_DATE',
                      'SHFTE_NOTES'
                     )

                cashier_shift_open_emp_writer = csv.DictWriter(cashier_shift_open_emp_file, fieldnames=cashier_shift_open_emp_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_open_emp_headers = dict( (n,n) for n in cashier_shift_open_emp_fields )
                cashier_shift_open_emp_writer.writerow(cashier_shift_open_emp_headers)
                
                ################# cashier_shift_closing
                
                cashier_shift_closing_fields = ('USE_BUS_UNIT', 'USE_SHIFT_PFX', 'USE_SHIFT_NO','USE_EMPLOYEE','USE_SHIFT_NO1','USE_SHIFT_DATE','USE_OPEN_AMT',
                      'USE_CASH_SALE','USE_CASH_INV','USE_CR_SALE','USE_CR_INV','USE_CASH_RET_SALE','USE_CASH_RET_INV','USE_CR_RET_SALE','USE_CR_RET_INV','USE_AIRLINE_SALE',
                      'USE_MANAGEMNET_SALE','USE_CLOSING_CASH','USE_CLOS_CASH_50H','USE_CLOS_CASH_1R','USE_CLOS_CASH_5R','USE_CLOS_CASH_10R',
                      'USE_CLOS_CASH_20R','USE_CLOS_CASH_50R','USE_CLOS_CASH_100R','USE_CLOS_CASH_500R','USE_FOR_CURR_AMT','USE_LEFT_CHANGE','USE_SHIFT_STATUS',
                      'USE_CRE_DATE','USE_CRE_BY','USE_NOTES','USE_COUNTER_NUMBER','USE_UNIT','USE_SUPERVISOR','USE_CLOSING_DATE',
                      'USE_CLOS_CASH_200R','USE_CLOS_CASH_1000R','USE_FIRST_INV','USE_LAST_INV','USE_NET_SALES','USE_SHIFT','USE_REASON','USE_NO','USE_TOTAL_SALES',
                      'USE_TOTAL_RETURN','USE_TOTAL_SALES_INV','USE_TOTAL_RETURN_INV','USE_NET_INV','USE_DIFF_AMT','USE_NET_DRAWER_AMT','USE_PETTY_CASH'
                     )

                cashier_shift_closing_writer = csv.DictWriter(cashier_shift_closing_file, fieldnames=cashier_shift_closing_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_closing_headers = dict( (n,n) for n in cashier_shift_closing_fields )
                cashier_shift_closing_writer.writerow(cashier_shift_closing_headers)
                
                ################# credit_card_header
                
                credit_card_hd_fields = ('SML4H_BUS_UNIT', 'SML4H_UNIT', 'SML4H_SHIFT_PFX','SML4H_SHIFT_NO','SML4H_TRANS_NO','SML4H_AMOUNT','SML4H_CRE_DT',
                      'SML4H_CRE_BY','SML4H_NOTES')

                credit_card_hd_writer = csv.DictWriter(credit_card_hd_file, fieldnames=credit_card_hd_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                credit_card_hd_headers = dict( (n,n) for n in credit_card_hd_fields )
                credit_card_hd_writer.writerow(credit_card_hd_headers)
                
                ################# credit_card_ln
                
                credit_card_ln_fields = ('SML4_BUS_UNIT', 'SML4_UNIT', 'SML4_SHIFT_PFX','SML4_SHIFT_NO','SML4_TRAN_NO','SML4_TYPE','SML4_AMOUNT',
                      'SML4_CRE_DT','SML4_CRE_BY','SML4_NOTES','SML4_QTY')

                credit_card_ln_writer = csv.DictWriter(credit_card_ln_file, fieldnames=credit_card_ln_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                credit_card_ln_headers = dict( (n,n) for n in credit_card_ln_fields )
                credit_card_ln_writer.writerow(credit_card_ln_headers)
                
                ################ supervisor closing
                
                supervisor_shift_closing_fields = ('SMH_BUS_UNIT', 'SMH_UNIT', 'SMH_SHIFT_PFX','SMH_SHIFT_NO','SMH_TOTAL_EMP','SMH_SHIFT_DATE','SMH_SHIFT',
                      'SMH_500_CNT','SMH_200_CNT','SMH_100_CNT','SMH_50_CNT','SMH_20_CNT','SMH_10_CNT','SMH_5_CNT','SMH_1_CNT',
                      'SMH_50H_CNT','SMH_LEFT_CHANGE','SMH_AIRLINES_CR','SMH_MANAGEMENT_CR','SMH_1000_CNT'
                     )

                supervisor_shift_closing_writer = csv.DictWriter(super_shift_closing_file, fieldnames=supervisor_shift_closing_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                supervisor_shift_closing_headers = dict( (n,n) for n in supervisor_shift_closing_fields )
                supervisor_shift_closing_writer.writerow(supervisor_shift_closing_headers)
                
                ################## Shift Tracking
                shift_tracking_fieldnames = ('MST_BUS_UNIT', 'MST_SHIFT_PFX', 'MST_SHIFT_NO','MST_HD','MST_LN','MST_PAY','MST_LND')

                shift_tracking_writer = csv.DictWriter(shifttrackingfile, fieldnames=shift_tracking_fieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                shift_tracking_headers = dict( (n,n) for n in shift_tracking_fieldnames )
                shift_tracking_writer.writerow(shift_tracking_headers)
                
                #################### logic to export csv files
                for super_session_date in session_dates:
                    
                    session_date = datetime.datetime.strptime(super_session_date, '%Y-%m-%d' )
                    search_start_date   = session_date.strftime('%Y-%m-%d %H:%M:%S')
                    search_end_date     = session_date.strftime('%Y-%m-%d 23:59:59')
                    
                    cashier_shift_obj = self.pool['pos.session']
                    cashier_shift_ids = cashier_shift_obj.search(cr,uid,[('start_at','>=',search_start_date),('start_at','<=',search_end_date)])
                    print super_session_date + "---> " + str(cashier_shift_ids)
                    order_hd_count  =   0
                    order_ln_count  =   0
                    order_py_count  =   0
                    order_lnd_count =   0
                    supervisor_mgt_credit = 0
                    supervisor_cash_sales = 0
                    supervisor_card_sales = 0
                    
                    card_wise_sales = {}
                    card_wise_no_sales = {}

                    if cashier_shift_ids:
                        #### Supervisor Shift Opening
                        cashier_shift_brws = cashier_shift_obj.browse(cr,uid,sorted(cashier_shift_ids))
                        super_shift_opening_dict = {}
                        fixed_super_shift_dict = {"SHFTH_BUS_UNIT":BUSINESS_UNIT,"SHFTH_UNIT":UNIT_ID,"SHFTH_PFX":SHIFT_PFX,
                                                 "SHFTH_SHIFT":"DAY","SHFTH_LEFT_CASH":0,"SHFTH_CRE_BY":UNIT_ID}
                        
                        sv_date = datetime.datetime.strptime(cashier_shift_brws[0].start_at, '%Y-%m-%d %H:%M:%S' )
                        super_shift_opening_dict['SHFTH_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        super_shift_opening_dict['SHFTH_YEAR'] = sv_date.strftime('%Y')
                        super_shift_opening_dict['SHFTH_PERIOD'] = sv_date.strftime('%m')
                        super_shift_opening_dict['SHFTH_SV_EMPLOYEE'] = SUPERVISOR
                        super_shift_opening_dict['SHFTH_PASSWORD'] = SUPERVISOR_PWD
                        super_shift_opening_dict['SHFTH_POST'] = "C"
                        super_shift_opening_dict['SHFTH_CRE_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        super_shift_opening_dict['SHFTH_NOTES'] = ""
                        
                        super_shift_opening_dict['SHFTH_NO'] = self.pool.get('ir.sequence').get(cr, uid, 'supervisor.shift')
                        
                        super_shift_opening_dict.update(fixed_super_shift_dict)
                        super_shift_opening_writer.writerow(super_shift_opening_dict)
                        ####
                        supervisor_left_change = 0
                        #### Cashier Shift
                        csb_ln = 1
                        for csb in cashier_shift_brws:
                            #### Cashier Shift Opening
                            cashier_left_change = 0
                            mgt_credit = 0
                            mgt_credit_count = 0
                            cashier_shift_opening_dict = {}
                            fixed_cashier_shift_dict = {"SHFTO_BUS_UNIT":BUSINESS_UNIT,"SHFTO_PFX":SHIFT_PFX,"SHFTO_EXCHANGE_RATE":"1",
                                                     "SHFTO_PAYMENT":"CASH","SHFTO_CURRENCY":"DEFLT","SHFTO_CRE_BY":UNIT_ID,
                                                     "SHFTO_AMT":"0","SHFTO_AMT_GL":"0"
                                                     }
                            
                            ca_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S' )
                            cashier_shift_opening_dict['SHFTO_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_opening_dict['SHFTO_EMP'] = csb.user_id.emis_id
                            cashier_shift_opening_dict['SHFTO_LN'] = csb_ln
                            cashier_shift_opening_dict['SHFTO_EXCHANGE_DATE'] = ca_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_opening_dict['SHFTO_CRE_DATE'] = ca_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_opening_dict['SHFTO_NOTES'] = ""
                            
                            cashier_shift_opening_dict.update(fixed_cashier_shift_dict)
                            cashier_shift_opening_writer.writerow(cashier_shift_opening_dict)
                            ####
                        
                            #### Cashier Shift EMP
                            cashier_shift_emp_dict = {}
                            fixed_cashier_shift_emp_dict = {"SHFTE_BUS_UNIT":BUSINESS_UNIT,"SHFTE_PFX":SHIFT_PFX,"SHFTE_POST":"O",
                                                        "SHFTE_CRE_BY":UNIT_ID,
                                                     }
                            
                            cae_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S' )
                            cashier_shift_emp_dict['SHFTE_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_emp_dict['SHFTE_EMP'] = csb.user_id.emis_id
                            cashier_shift_emp_dict['SHFTE_CRE_DATE'] = cae_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_emp_dict['SHFTE_NOTES'] = ""
                            
                            cashier_shift_emp_dict.update(fixed_cashier_shift_emp_dict)
                            cashier_shift_open_emp_writer.writerow(cashier_shift_emp_dict)
                            ####
                            pos_order_obj = self.pool['pos.order']
                            pos_orders = pos_order_obj.search(cr,uid,[('session_id','=',csb.id)])
                            orders_brws = []
                            if pos_orders:
                                orders_brws = pos_order_obj.browse(cr,uid,sorted(pos_orders))
                            
                            
                            tendered_amount = 0
                            journal_wise_amount_in = {}
                            journal_wise_amount_out = {}
                            journal_wise_total = {}
                            
                            #### Sale order Header 
                            for res in orders_brws:
                                left_change = 0.0
                                sale_dict = {}
                                
                                fixed_sale_dict = {"SOH_BUS_UNIT":BUSINESS_UNIT,"SOH_PFX":SALES_PFX ,"SOH_FROM":"UNIT" ,
                                              "SOH_CODE" : UNIT_ID,"SOH_SALES_TYPE":"I","SOH_CUSTOMER":"DEFLT","SOH_CUSTOMER_NAME":"DEFLT",
                                              "SOH_CUSTOMER_EMP":"DEFLT","SOH_TERMS":"DEFLT","SOH_CURRENCY":"DEFLT","SOH_EXCHANGE_RATE":"1",
                                              "SOH_SALESPERSON":"DEFLT","SOH_WHSE":UNIT_ID,"SOH_POST":"N","SOH_DISC_PCT":"0","SOH_DISC_AMT":"0",
                                              "SOH_INV_DISC_FLAG":"N","SOH_INV_DISC_PCT":"0","SOH_LEFT_CASH":"0","SOH_LEFT_RETURN":"N",
                                              "SOH_CASH_GROUP":"P","SOH_CRE_BY":UNIT_ID,"SOH_SHIFT_PFX":SHIFT_PFX}
                                
                                ### type of sale (sales/return), payment type (cash/Credit)
                                sale_dict['SOH_TYPE'] = "C"
                                sale_dict['SOH_CLS'] = "N"
                                
                                val1 =  0.0
                                for line in res.lines:
                                    val1 += line.qty * line.price_unit
                                
                                if res.amount_total == 0 and val1>0:
                                    mgt_credit += val1
                                    mgt_credit_count +=1
                                    supervisor_mgt_credit +=val1
                                    sale_dict['SOH_TYPE'] = "T"
                                
                                stmt_ids = res.statement_ids
                                for stmt in res.statement_ids:
                                    if stmt.amount < 0 and stmt.pos_statement_id.order_parent_id:
                                        sale_dict['SOH_CLS'] = "C"
                                        sale_dict["SOH_RET_BUS_UNIT"] = BUSINESS_UNIT
                                        sale_dict["SOH_RET_UNIT"] = UNIT_ID
                                        sale_dict["SOH_RET_INV_PFX"] = RETURN_PFX
                                        sale_dict["SOH_RET_INV_NO"] = stmt.pos_statement_id.order_parent_id.id
                                    else:
                                        sale_dict['SOH_CLS'] = "N"
                                        
                                    if(stmt.amount > 0) and stmt.journal_id.name == 'Credit Management':
                                        sale_dict['SOH_TYPE'] = "T"
                                
                                sale_dict["SOH_NO"] = res.id or "" ###change the no
                                d = datetime.datetime.strptime(res.date_order, '%Y-%m-%d %H:%M:%S' )
                                sale_dict["SOH_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_YEAR"] = d.strftime('%Y')
                                sale_dict["SOH_PERIOD"] = d.strftime('%m')
                                sale_dict["SOH_CUSTOMER"] = res.partner_id.emis_id or "DEFLT"
                                sale_dict["SOH_CUSTOMER_NAME"] = res.partner_id and res.partner_id.name or ""
                                sale_dict["SOH_EXCHANGE_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_EMPLOYEE"] = csb.user_id.emis_id or ""
                                sale_dict["SOH_SHIFT_NO"] = super_shift_opening_dict['SHFTH_NO']
                                
                                sale_dict["SOH_EXCLUDE_CARD_CHRG"] = "N"
                                sale_dict["SOH_CRE_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_NOTES"] = "C" ###customer C , Staff S
                                sale_dict["SOH_CREDIT_HOLD"] = "D"
                                sale_dict["SOH_HOLD_REASON"] = ""
                                sale_dict["SOH_OVER_DUE_AMT"] = "0"
                                sale_dict["SOH_PRINT_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_AR_DOC_PFX"]=""
                                sale_dict["SOH_AR_DOC_NO"] = ""
                                sale_dict["SOH_DISC_LOCK"] = "O"
                                sale_dict["SOH_REFRESHED"] = "N"
                                sale_dict["SOH_ALLOW_MODIFY"]= "Y"
                                sale_dict["SOH_REFRESH_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_SALESMAN"] = csb.user_id.emis_id or ""
                                sale_dict["SOH_DIR_DEL_FLAG"] = "N"
                                sale_dict["SOH_SV_CUSTOMER"] = ""
                                sale_dict["SOH_PIN_CODE"]= ""
                                sale_dict["SOH_CENTRAL_DISC_CARD"]= ""
                                sale_dict["SOH_COUNTER_NUMBER"] = "1" ### counter no
                                sale_dict["SOH_SHIFT_NUMBER"] = csb_ln  ###cashier shift no autogenerated 
                                
                                sale_dict.update(fixed_sale_dict)
                                writer.writerow(sale_dict)
                                ###end of sale order
                                
                                ## start of sale order lines
                                counter = 1
                                for line in res.lines:
                                    if line.product_id.name == 'Tip':
                                        left_change += (line.price_unit * line.qty)
                                        continue
                                    
                                    if line.qty < 1:
                                        continue
                                    line_dict = {}
                                    line_detail_dict = {}
                                    fixed_line_dict = {"SOL_BUS_UNIT":BUSINESS_UNIT,"SOL_PFX":SALES_PFX,"SOL_FACTOR":"1" ,"SOL_COST":"0" ,
                                              "SOL_DISC_PCT" : "0","SOL_FREE_QTY":"0","SOL_CNS_NO":"0",
                                              "SOL_CNS_LN":"0","SOL_DISC_AMT":"0", "SOL_CRE_BY":UNIT_ID,"SOL_WHSE":UNIT_ID,
                                              "SOL_ITEM_REV":"0",
                                              }
                                    
                                    fixed_linedetail_dict = {"SOLD_BUS_UNIT":BUSINESS_UNIT,"SOLD_UNIT":UNIT_ID,"SOLD_WHSE":UNIT_ID,"SOLD_PFX":SALES_PFX,
                                                             "SOLD_FACTOR":"1","SOLD_COST":"0","SOLD_BOM_COST":"0","SOLD_ITEM_REV":"0", "SOLD_BOM_ITEM_REV":"0"
                                              }
                                    
                                    ### start logic for sale order line
                                    line_dict['SOL_NO'] = res.id or ""
                                    line_dict['SOL_LN'] = counter
                                    line_dict['SOL_BAR_CODE'] = line.product_id.default_code or ""
                                    line_dict['SOL_ITEM'] = line.product_id.default_code or ''
                                    
                                    line_detail_dict['SOLD_LN'] =   counter
                                    line_detail_dict['SOLD_NO'] =   res.id
                                    line_detail_dict['SOLD_SHIFT_NO'] = super_shift_opening_dict['SHFTH_NO'] ###dynamic cashier shift no
                                    
                                    line_detail_dict["SOLD_BAR_CODE"] = line.product_id.default_code or ""
                                    line_detail_dict["SOLD_ITEM"] = line.product_id.default_code or ""
                                    line_detail_dict["SOLD_ITEM_CAT"] = "90"
                                    line_detail_dict["SOLD_QTY"] = line.qty
                                    line_detail_dict["SOLD_UOM"] =  "PCS" #line.product_id.uom_id.name
                                    line_detail_dict["SOLD_UOM_STOCK"] = "PCS" #line.product_id.uom_po_id.name or 
                                    line_detail_dict["SOLD_PRICE"] = line.price_unit
                                    line_detail_dict["SOLD_TYPE"] = sale_dict['SOH_TYPE']
                                    line_detail_dict["SOLD_CLS"] = sale_dict['SOH_CLS']
                                    line_detail_dict["SOLD_SHIFT_PFX"] = SHIFT_PFX
                                    line_detail_dict["SOLD_SHIFT_DATE"] = sale_dict["SOH_DATE"]
                                    line_detail_dict["SOLD_YEAR"] = sale_dict["SOH_YEAR"]
                                    line_detail_dict["SOLD_PERIOD"] = sale_dict["SOH_PERIOD"]
                                    
                                    bom_sequence = 1
                                    
                                    bom_details = self.pool.get("product.bom.detail").search(cr,uid,[('parent_id','=',line.product_id.id)])
                                    bom_details_brws = self.pool.get("product.bom.detail").browse(cr,uid,bom_details)
                                    
#                                     for bom_detail in bom_details_brws:
#                                         #if not bom_detail.has_choice:
#                                         if True:
#                                             line_detail_dict['SOLD_LN1'] = bom_sequence
#                                             line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                             line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                             line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                             line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                             line_detail_dict.update(fixed_linedetail_dict)
#                                             linedetail_writer.writerow(line_detail_dict)
#                                             order_lnd_count +=1
#                                             bom_sequence +=1
#                                         else:
#                                             for dtl in line.line_detail:
#                                                 line_detail_dict['SOLD_LN1'] = bom_sequence
#                                                 line_detail_dict["SOLD_BOM_ITEM"] = dtl.product_id.default_code or ""
#                                                 line_detail_dict["SOLD_BOM_UOM"] = dtl.uom
#                                                 line_detail_dict["SOLD_BOM_QTY"] = dtl.qty
#                                                 line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * dtl.qty
#                                                 line_detail_dict.update(fixed_linedetail_dict)
#                                                 linedetail_writer.writerow(line_detail_dict)
#                                                 order_lnd_count +=1
#                                                 bom_sequence +=1
                                    
#                                             notes_list = []
#                                             if line.note:
#                                                 notes_list = [tuple(ln_note.split(' - ')) for ln_note in line.note.split("\n")]
#                                             
#                                             prod_tmpl_obj = self.pool.get("product.template")
#                                             
#                                             if not notes_list:
#                                                 line_detail_dict['SOLD_LN1'] = bom_sequence
#                                                 line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                                 line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                                 line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                                 line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                                 print "inside default choice item"
#                                                 line_detail_dict.update(fixed_linedetail_dict)
#                                                 linedetail_writer.writerow(line_detail_dict)
#                                                 order_lnd_count +=1
#                                                 bom_sequence +=1
#                                             
#                                             for line_note in notes_list:
#                                                 print line_note
#                                                 if len(line_note)<>2 or not isinstance(line_note[1], int):
#                                                     line_detail_dict['SOLD_LN1'] = bom_sequence
#                                                     line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                                     line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                                     line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                                     line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                                     print "inside default choice item"
#                                                     line_detail_dict.update(fixed_linedetail_dict)
#                                                     linedetail_writer.writerow(line_detail_dict)
#                                                     order_lnd_count +=1
#                                                     bom_sequence +=1
#                                                     continue
#                                                 line_detail_dict['SOLD_LN1'] = bom_sequence
#                                                 comp_product_id = prod_tmpl_obj.search(cr,uid,[('name','=',line_note[1] if len(line_note) > 1 else False)])
#                                                 line_detail_dict["SOLD_BOM_ITEM"] = prod_tmpl_obj.browse(cr,uid,comp_product_id).default_code or "" 
#                                                 line_detail_dict["SOLD_BOM_QTY"] = float(line_note[0])
#                                                 line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                                 line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * float(line_note[0])
#                                                 print "inside notes list"
#                                                 line_detail_dict.update(fixed_linedetail_dict)
#                                                 linedetail_writer.writerow(line_detail_dict)
#                                                 order_lnd_count +=1
#                                                 bom_sequence +=1
                                        
                                    
                                    line_dict['SOL_QTY'] = line.qty
                                    line_dict['SOL_UOM'] = "PCS" ##line.product_id.uom_id.name ##update the UOM in Odoo
                                    line_dict['SOL_UOM_STOCK'] = "PCS" ##line.product_id.uom_po_id.name ##update the UOM in Odoo
                                    line_dict['SOL_PRICE'] = line.price_unit
                                    line_dict['SOL_CRE_DATE'] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    line_dict['SOL_NOTES'] = ""
                                    line_dict['SOL_SALESMAN'] = csb.user_id.emis_id or ''
                                    counter = counter + 1
                                    
                                    #print line.qty * line.price_unit
                                    
                                    ### end of logic for sale order lines 
                                    line_dict.update(fixed_line_dict)
                                    line_writer.writerow(line_dict)
                                    
                                    order_ln_count +=1
                                    
                                ## end of sale order lines 
                                
                                ## start of payment lines
                                
                                for payment in stmt_ids:
                                    if payment.journal_id.name == "Credit Management":
                                        continue
                                    if payment.amount > 0:
                                        journal_wise_amount_in[payment.journal_id.id] = (journal_wise_amount_in.get(payment.journal_id.id)+payment.amount)  if journal_wise_amount_in.get(payment.journal_id.id,False) else payment.amount
                                        tendered_amount += abs(payment.amount)
                                        journal_wise_total[payment.journal_id.id] = (journal_wise_total.get(payment.journal_id.id)+payment.amount)  if journal_wise_total.get(payment.journal_id.id,False) else payment.amount
                                    elif payment.amount < 0:
                                        journal_wise_amount_out[payment.journal_id.id] = (journal_wise_amount_out.get(payment.journal_id.id)+payment.amount)  if journal_wise_amount_out.get(payment.journal_id.id,False) else payment.amount
                                        journal_wise_total[payment.journal_id.id] = (journal_wise_total.get(payment.journal_id.id)+payment.amount)  if journal_wise_total.get(payment.journal_id.id,False) else payment.amount

                                payment_sequence = 0
                                for key, value in journal_wise_total.iteritems():
                                    ### start logic for payment lines
                                    payment_sequence +=1
                                    payment_dict = {}
                                    fixed_payment_dict = {"PAY_BUS_UNIT"  : BUSINESS_UNIT,"PAY_PFX":SALES_PFX,"PAY_CRE_BY":UNIT_ID}
                                    payment_dict["PAY_NO"]= res.id
                                    payment_dict["PAY_LN"]= payment_sequence ###sequences
                                    payment_dict["PAY_PAY"]= self.pool.get('account.journal').browse(cr,uid,key).emis_id
                                    payment_dict["PAY_CURRENCY"]= "DEFLT"
                                    payment_dict["PAY_EXCHANGE_DATE"]= d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    payment_dict["PAY_EXCHANGE_RATE"]= "1"
                                    payment_dict["PAY_FC_VALUE"]= journal_wise_amount_in.get(key,0)
                                    payment_dict["PAY_AMT"]= journal_wise_amount_in.get(key,0)
                                    payment_dict["PAY_CRED_CARD_CHRG"]= ""
                                    payment_dict["PAY_AMT_GL"]= res.amount_total - left_change
                                    payment_dict["PAY_LC_CHANGE"]= abs(journal_wise_amount_out.get(key,0))
                                    payment_dict["PAY_CARD_NO"]= ""
                                    payment_dict["PAY_CARD_TRANS_NO"]= ""
                                    
                                    payment_dict["PAY_PERSON_NAME"]=""
                                    payment_dict["PAY_NATIONALITY"]=""
                                    payment_dict["PAY_PASSPORT_NO"]=""
                                    payment_dict["PAY_FLIGHT_NO"]=""
                                    payment_dict["PAY_CHECK_NO"]=""
                                    payment_dict["PAY_CHECK_DATE"]=""
                                    
                                    payment_dict["PAY_CRE_DATE"]= d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    payment_dict["PAY_NOTES"]= ""
                                    
                                    ### end of logic for payment lines
                                    payment_dict.update(fixed_payment_dict)
                                    payment_writer.writerow(payment_dict)
                                    
                                    order_py_count +=1
                                    
                                ## end of payment lines
                                
                                order_hd_count +=1
                                cashier_left_change +=left_change 
                        
                            #### Cashier Shift Closing
                            
                            cash_sale_amount = 0
                            cash_sale_return_amount = 0
                            
                            cash_sale_invoice_ids = []
                            cash_sale_order_count = 0
                            
                            change_return_amount = 0
                            
                            cash_sale_return_invoice_ids = []
                            cash_sale_return_order_count = 0
                            
                            card_sale_amount = 0
                            card_sale_order_count = 0
                            card_sale_invoice_ids = []
                            
                            credit_sale_amount = 0
                            credit_sale_return_amount = 0
                            
                            credit_sale_invoice_ids = []
                            credit_sale_order_count = 0
                            
                            credit_sale_return_invoice_ids = []
                            credit_sale_return_order_count = 0
                            
                            for st in csb.statement_ids:
                                if st.balance_end:
                                    card_wise_sales[st.journal_id.id] = (card_wise_sales.get(st.journal_id.id)+st.balance_end)  if card_wise_sales.get(st.journal_id.id,False) else st.balance_end
                                if st.journal_id.type == 'cash':
                                    for st_line in st.line_ids:
                                        if (st_line.amount < 0 and st_line.pos_statement_id.order_parent_id):
                                            cash_sale_return_invoice_ids.append(st_line.pos_statement_id.id)
                                            cash_sale_return_order_count +=1
                                            cash_sale_return_amount += abs(st_line.amount)
                                        elif (st_line.amount < 0 and ": return" in st_line.name):
                                            change_return_amount += abs(st_line.amount)
                                        elif st_line.amount > 0 and ("Main" in st_line.name or "Order" in st_line.name):
                                            cash_sale_invoice_ids.append(st_line.pos_statement_id.id)
                                            cash_sale_amount += abs(st_line.amount)
                                            #print cash_sale_amount
                                            cash_sale_order_count +=1
                                elif st.journal_id.type == 'bank':
                                    if st.journal_id.name == "Credit Management":
                                        for st_line in st.line_ids:
                                            if st_line.amount > 0 and ("Main" in st_line.name or "Order" in st_line.name):
                                                mgt_credit += abs(st_line.amount)
                                                mgt_credit_count +=1
                                                supervisor_mgt_credit += abs(st_line.amount)
                                                credit_sale_amount += abs(st_line.amount)
                                                credit_sale_order_count +=1
                                                credit_sale_invoice_ids.append(st_line.pos_statement_id.id)
                                    else:
                                        for st_line in st.line_ids:
                                            if st_line.amount > 0 and ("Main" in st_line.name or "Order" in st_line.name):
                                                card_wise_no_sales[st.journal_id.id] = (card_wise_no_sales.get(st.journal_id.id)+1)  if card_wise_no_sales.get(st.journal_id.id,False) else 1
                                                card_sale_amount += abs(st_line.amount)
                                                print card_sale_amount
                                                card_sale_order_count +=1
                                                card_sale_invoice_ids.append(st_line.pos_statement_id.id)
                            
                            cashier_shift_close_dict = {}
                            fixed_cashier_shift_close_dict = {"USE_BUS_UNIT":BUSINESS_UNIT,"USE_SHIFT_PFX":SHIFT_PFX,"USE_UNIT":UNIT_ID,
                                                        "USE_CRE_BY":UNIT_ID }
                            
                            cac_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S')
                            cac_stop_date = datetime.datetime.strptime(csb.stop_at, '%Y-%m-%d %H:%M:%S')
                            cashier_shift_close_dict['USE_SHIFT_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_close_dict['USE_EMPLOYEE'] = csb.user_id.emis_id
                            cashier_shift_close_dict['USE_SHIFT_NO1'] = csb_ln
                            cashier_shift_close_dict['USE_SHIFT_DATE'] = cac_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            
                            cashier_shift_close_dict['USE_OPEN_AMT'] = supervisor_cash_sales
                            cashier_shift_close_dict['USE_CASH_SALE'] = (cash_sale_amount - change_return_amount) + card_sale_amount - cashier_left_change
                            cashier_shift_close_dict['USE_CASH_INV'] = len(set(cash_sale_invoice_ids + card_sale_invoice_ids))
                            
                            cashier_shift_close_dict['USE_CR_SALE'] = mgt_credit
                            cashier_shift_close_dict['USE_CR_INV'] = mgt_credit_count
                            
                            cashier_shift_close_dict['USE_CASH_RET_SALE'] = cash_sale_return_amount
                            cashier_shift_close_dict['USE_CASH_RET_INV'] = len(set(cash_sale_return_invoice_ids))
                            
                            cashier_shift_close_dict['USE_CR_RET_SALE'] = "0"
                            cashier_shift_close_dict['USE_CR_RET_INV'] = "0"
                            
                            cashier_shift_close_dict['USE_AIRLINE_SALE'] = "0"
                            cashier_shift_close_dict['USE_MANAGEMNET_SALE'] = mgt_credit
                            
                            cashier_shift_close_dict['USE_CLOSING_CASH'] = (cash_sale_amount - change_return_amount)-cash_sale_return_amount - cashier_left_change
                            
                            cashier_shift_close_dict['USE_CLOS_CASH_50H'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_1R'] = (cash_sale_amount - change_return_amount)-cash_sale_return_amount - cashier_left_change
                            cashier_shift_close_dict['USE_CLOS_CASH_5R'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_10R'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_20R'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_50R'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_100R'] = "0"
                            cashier_shift_close_dict['USE_CLOS_CASH_500R'] = "0"
                            
                            cashier_shift_close_dict['USE_FOR_CURR_AMT'] = "0" ##no foreign currency
                            cashier_shift_close_dict['USE_LEFT_CHANGE'] = "0"
                            
                            cashier_shift_close_dict['USE_SHIFT_STATUS'] = "C"
                            
                            cashier_shift_close_dict['USE_CRE_DATE'] = cac_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_close_dict['USE_NOTES'] = ""
                            
                            cashier_shift_close_dict['USE_COUNTER_NUMBER'] = "1" ##Need to dynamic
                            
                            cashier_shift_close_dict['USE_SUPERVISOR'] = SUPERVISOR
                            
                            cashier_shift_close_dict['USE_CLOSING_DATE'] = cac_stop_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            
                            cashier_shift_close_dict['USE_CLOS_CASH_200R'] = ""
                            cashier_shift_close_dict['USE_CLOS_CASH_1000R'] = card_sale_amount
                            
                            cashier_shift_close_dict['USE_FIRST_INV'] = ""
                            cashier_shift_close_dict['USE_LAST_INV'] = ""
                            
                            cashier_shift_close_dict['USE_NET_SALES'] = ((cash_sale_amount - change_return_amount) + card_sale_amount + mgt_credit) - (cash_sale_return_amount + credit_sale_return_amount) - cashier_left_change
                            
                            cashier_shift_close_dict['USE_SHIFT'] = "DAY" ### Need to be dynamic
                            cashier_shift_close_dict['USE_REASON'] = "Final Close"
                            
                            cashier_shift_close_dict['USE_NO'] = "" ## Fixed
                            
                            cashier_shift_close_dict['USE_TOTAL_SALES'] = (cash_sale_amount - change_return_amount) + card_sale_amount + mgt_credit - cashier_left_change
                            cashier_shift_close_dict['USE_TOTAL_RETURN'] = abs(cash_sale_return_amount + credit_sale_return_amount)
                            
                            TOTAL_SALES_INV = cash_sale_invoice_ids + card_sale_invoice_ids
                            TOTAL_RETURN_INV = cash_sale_return_invoice_ids + credit_sale_return_invoice_ids
                            
                            cashier_shift_close_dict['USE_TOTAL_SALES_INV'] = len(set(TOTAL_SALES_INV)) + mgt_credit_count
                            cashier_shift_close_dict['USE_TOTAL_RETURN_INV'] = len(set(TOTAL_RETURN_INV)) ## based on return invoice
                            cashier_shift_close_dict['USE_NET_INV'] = len(set(TOTAL_SALES_INV)) + len(set(TOTAL_RETURN_INV)) + mgt_credit_count
                            
                            cashier_shift_close_dict['USE_DIFF_AMT'] = "0"
                            cashier_shift_close_dict['USE_NET_DRAWER_AMT'] = (cash_sale_amount - change_return_amount)-cash_sale_return_amount - cashier_left_change
                            cashier_shift_close_dict['USE_PETTY_CASH'] = "0"
                            
                            cashier_shift_close_dict.update(fixed_cashier_shift_close_dict)
                            cashier_shift_closing_writer.writerow(cashier_shift_close_dict)
                            csb_ln +=1
                            supervisor_cash_sales += ((cash_sale_amount - change_return_amount) - cash_sale_return_amount) - cashier_left_change
                            supervisor_card_sales += (card_sale_amount)
                            ##
                            supervisor_left_change +=cashier_left_change
                    
                        #### Supervisor Shift Closing
                        super_shift_closing_dict = {}
                        cc_transaction_hd_dict = {}
                        fixed_super_shift_close_dict = {"SMH_BUS_UNIT":BUSINESS_UNIT,"SMH_UNIT":UNIT_ID,"SMH_SHIFT_PFX":SHIFT_PFX}
                        
                        super_shift_closing_dict['SMH_SHIFT_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        super_shift_closing_dict['SMH_SHIFT_NO'] = super_shift_opening_dict['SHFTH_NO']
                        super_shift_closing_dict['SMH_1_CNT'] = supervisor_cash_sales
                        super_shift_closing_dict['SMH_LEFT_CHANGE'] = "0"
                        super_shift_closing_dict['SMH_AIRLINES_CR'] = "0"
                        super_shift_closing_dict['SMH_MANAGEMENT_CR'] = supervisor_mgt_credit
                        super_shift_closing_dict['SMH_1000_CNT'] = supervisor_card_sales
                        super_shift_closing_dict['SMH_SHIFT'] = "DAY"
                        super_shift_closing_dict['SMH_TOTAL_EMP'] = len(cashier_shift_ids)
                        
                        #### credit card hd
                        cc_transaction_hd_dict['SML4H_BUS_UNIT']    =  BUSINESS_UNIT
                        cc_transaction_hd_dict['SML4H_UNIT']        =  UNIT_ID
                        cc_transaction_hd_dict['SML4H_SHIFT_PFX']   =  SHIFT_PFX
                        
                        cc_transaction_hd_dict['SML4H_SHIFT_NO']    =   super_shift_opening_dict['SHFTH_NO']
                        cc_transaction_hd_dict['SML4H_TRANS_NO']    =   super_shift_opening_dict['SHFTH_NO']
                        cc_transaction_hd_dict['SML4H_AMOUNT']      =   supervisor_card_sales
                        cc_transaction_hd_dict['SML4H_CRE_DT']      =   sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        cc_transaction_hd_dict['SML4H_CRE_BY']      =   UNIT_ID
                        cc_transaction_hd_dict['SML4H_NOTES']       =   ""
                        
                        credit_card_hd_writer.writerow(cc_transaction_hd_dict)
                        
                        for key, value in card_wise_sales.iteritems():
                            #### credit card ln
                            payment_type = self.pool.get('account.journal').browse(cr,uid,key)
                            if value and key in card_ids:
                                cc_transaction_ln_dict = {}
                                cc_transaction_ln_dict['SML4_BUS_UNIT']     =   BUSINESS_UNIT
                                cc_transaction_ln_dict['SML4_UNIT']         =   UNIT_ID
                                cc_transaction_ln_dict['SML4_SHIFT_PFX']    =   SHIFT_PFX
                                
                                cc_transaction_ln_dict['SML4_SHIFT_NO']     =   super_shift_opening_dict['SHFTH_NO']
                                cc_transaction_ln_dict['SML4_TRAN_NO']      =   super_shift_opening_dict['SHFTH_NO']
                                cc_transaction_ln_dict['SML4_TYPE']         =   payment_type.emis_id or ""
                                cc_transaction_ln_dict['SML4_AMOUNT']       =   card_wise_sales[key]
                                cc_transaction_ln_dict['SML4_CRE_DT']       =   sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                                cc_transaction_ln_dict['SML4_CRE_BY']       =   UNIT_ID
                                cc_transaction_ln_dict['SML4_NOTES']        =   ""
                                cc_transaction_ln_dict['SML4_QTY']          =   card_wise_no_sales[key]
                            
                                credit_card_ln_writer.writerow(cc_transaction_ln_dict)
                        
                        super_shift_closing_dict.update(fixed_super_shift_close_dict)
                        supervisor_shift_closing_writer.writerow(super_shift_closing_dict)
                        
                        #### Supervisor Shift Tracking
                        shift_tracking_dict = {}
                        shift_tracking_dict["MST_SHIFT_NO"] = super_shift_opening_dict['SHFTH_NO']
                        shift_tracking_dict["MST_BUS_UNIT"] = BUSINESS_UNIT
                        shift_tracking_dict["MST_SHIFT_PFX"] = SHIFT_PFX
                        shift_tracking_dict['MST_HD'] = str(order_hd_count)
                        shift_tracking_dict['MST_LN'] = str(order_ln_count)
                        shift_tracking_dict['MST_LND'] = str(order_lnd_count)
                        shift_tracking_dict['MST_PAY'] = str(order_py_count)
                    
                        shift_tracking_writer.writerow(shift_tracking_dict)
            finally:
                salecsvfile.close()
                linecsvfile.close()
                paymentcsvfile.close()
                linedetailfile.close()
                shifttrackingfile.close()
                super_shift_closing_file.close()
                super_shift_opening_file.close()
                cashier_shift_opening_file.close()
                cashier_shift_open_emp_file.close()
                cashier_shift_closing_file.close()
                credit_card_hd_file.close()
                credit_card_ln_file.close()
        
        inputfiles = ["OL1_MF_SO_HD_TMP.csv","OL1_MF_SO_LN_TMP.csv","OL1_MF_SO_LN_DETAIL_TMP.csv",
                          "OL1_MF_SO_PAY_TMP.csv","OL1_MF_SHIFT_TRACKING_TMP.csv","OL1_MF_SHIFT_MANHOUR_HD_TMP.csv",
                          "OL1_MF_UNIT_SHIFT_HD_TMP.csv","OL1_MF_UNIT_SHIFT_OPEN_TMP.csv","OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv",
                          "OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv","OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv","OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv"]
        
        inputfiles_dict = {"OL1_MF_SO_HD_TMP.csv":"HD.txt","OL1_MF_SO_LN_TMP.csv":"LN.txt","OL1_MF_SO_LN_DETAIL_TMP.csv":"LND.txt",
                          "OL1_MF_SO_PAY_TMP.csv":"PY.txt","OL1_MF_SHIFT_TRACKING_TMP.csv":"ST.txt","OL1_MF_SHIFT_MANHOUR_HD_TMP.csv":"MAN1.txt",
                          "OL1_MF_UNIT_SHIFT_HD_TMP.csv":"S_HD.txt","OL1_MF_UNIT_SHIFT_OPEN_TMP.csv":"S_OP.txt","OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv":"S_OP_EMP.txt",
                          "OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv":"SH1.txt","OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv":"MAN5.txt","OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv":"MAN6.txt"}
#         for infile in inputfiles:
#             infile_list = infile.split('_TMP')
#             outfile = "".join(infile_list)
#             print infile, outfile
#             with open(infile,'r') as f:
#                 with open(outfile,'w') as f1:
#                     f.next() # skip header line
#                     for line in f:
#                         f1.write(line)   
        for infile,outfile in inputfiles_dict.iteritems():
            with open(infile,'r') as f:
                with open(outfile,'w') as f1:
                    f.next() # skip header line
                    for line in f:
                        f1.write(line)      
            
        print "export ends"
                ### export ends
        